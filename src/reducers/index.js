import { combineReducers } from 'redux';

import libraries from './library-reducer';
import libraryId from './selection-reducer';

export default combineReducers({
  libraries,
  selectedLibraryId: libraryId,
});
